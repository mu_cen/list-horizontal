# ListHorizontal

#### 介绍
鸿蒙系统，ArkTS-UI开发，自定义List列表横向滑动，列表停止时，列表中间元素显示在屏幕中间，滑动或点击中间左则，右则互动，自动滚动到中间位置动画效果。
  
 效果图
![输入图片说明](image.png)
![输入图片说明](%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_1.pngWeChat_20240320165700.gif)



#### 软件架构
软件架构说明:使用组件List，list循环使用懒加载LazyForEach方法(实现功能简单，不用下载了，查看文档Index.ets即可)


#### 使用说明

 功能实现：处理List监听onScrollStop()方法，实现逻辑


      List({ scroller: this.scroller }) {
        LazyForEach(this.data, (item, index) => {
          ListItem() {
            Row() {
              Text((index + 1)
                .toString())
                .fontSize(25)
            }
            .scale((index == this.showPos - 1) ? { x: 1, y: 1 } : { x: 0.9, y: 0.9 })//中间位置放大效果
            .animation({ duration: 300, curve: Curve.Smooth }) // 滑动动画
            .justifyContent(FlexAlign.Center)
            .width(this.cardWidth)
            .height(this.cardHeight)
            .backgroundColor(item.bgColor)
            .borderRadius(15)
            .onClick(() => {
              this.isOnTouch = false
              if (!this.isClick) {
                this.isClick = true
                if (index < this.showPos - 1) { //小于(点击左侧)
                  let sValue = this.showPos - 1 - index
                  let carW = (this.cardWidth + this.cardSpace) * sValue
                  this.setScrollOffset(this.scroller.currentOffset().xOffset - carW);
                  this.showPos = this.showPos - sValue
                } else if (index > this.showPos - 1) { //大于(点右左侧)
                  let sValue = index - (this.showPos - 1)
                  let carW = (this.cardWidth + this.cardSpace) * sValue
                  this.setScrollOffset(this.scroller.currentOffset().xOffset + carW);
                  this.showPos = this.showPos + sValue
                } else { //等于不处理

                }
                console.error('indexEScrollLogA','点击-当前位置：'+this.showPos)
                setTimeout(() => {
                  this.isClick = false
                }, 500)
              }
            })
          }
        }, (item) => item)
      }
      .onAppear(() => {
        setTimeout(() => {
          this.scroller.scrollTo({ xOffset: this.cardOffset, yOffset: 0 })
        }, 20)
      })
      .listDirection(Axis.Horizontal)
      .alignListItem(ListItemAlign.Center)
      .width('100%')
      .height('100%')
      .divider({ strokeWidth: this.cardSpace, color: Color.Transparent })
      .onTouch((event?: TouchEvent) => {
        if (event) {
          this.isOnTouch = false
          if (event.type == TouchType.Up) {
            //多次滑动，只有最后一次滑动抬起调值，onScrollStop方法执行
            this.isOnTouch = true
          }
        }
      })
      .onScrollStop(() => {
        if (this.isOnTouch) {
          this.isOnTouch = false
          //根据列表x轴偏移量，算出中间元素需要补位与退位值
          let xOffsetTemp = this.scroller.currentOffset().xOffset
          let xOffset = xOffsetTemp - this.initOffset
          let runX = xOffset / (this.cardWidth + 10)

          let numStr = runX.toString()
          let show = parseInt(numStr)//取整数，获取当前显示位置
          this.curShowPos = show + 3
          let dotIndex = numStr.indexOf('.')
          //只要小数点后数据，进行向前或向后移动
          if (dotIndex === -1) {
            //没有小数，不处理。
          } else {
            let numStrTemp = '0.' + numStr.slice(dotIndex + 1).toString()
            let numTemp = parseFloat(numStrTemp)
            if (numTemp > 0.5) { //大于0.5向前移动
              let offsetValue = (1 - parseFloat(numStrTemp)) * (this.cardWidth + this.cardSpace)
              this.setScrollOffset(this.scroller.currentOffset().xOffset + offsetValue);
              this.curShowPos = this.curShowPos + 1
            } else { //小于0.5向后移动
              let offsetValue = parseFloat(numStrTemp) * (this.cardWidth + this.cardSpace)
              this.setScrollOffset(this.scroller.currentOffset().xOffset - offsetValue);
            }
          }
          //设置屏幕中间位置，更新中间位置放大效果
          this.showPos = this.curShowPos
          console.error('indexEScrollLogA','滑动后-当前位置：'+this.showPos)
        }
      })
    }
    .width('100%')
    .height('30%')
    .backgroundColor(Color.Yellow)
  }

 
//实现组件
#### 约束与限制

1.  DevEco Studio 版本：4.0

2.  OpenHarmony SDK:API 9 


#### 目录结构

|---- 项目
    |---- entry# 示例代码文件夹      
    |     |---- src
    |          |---- main
    |                |---- ets          
    |                      |---- entryability # 库文件夹               
    |                           |---- EntryAbility.ts 
    |                      |---- pages# 库文件夹               
    |                           |---- Index.ets# 组件->实现列表横向滑动效果 
    |     |---- README.md  # 安装使用方法

#### 贡献代码
  自己实现该功能，目前够使用，可根据自己需求进行该动。使用过程中发现问题,可以发送邮件1002427098@qq.com。
